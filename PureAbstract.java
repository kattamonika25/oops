interface Video {
   public void review();
}
class Movie implements Video {
    String title;
    double rating;
        
    Movie(String t, double r ) {
        title =  t;
        rating = r;
    }
        
    public void review() {
        System.out.println(title + " is an amazing movie with a rating of " + rating + " out of 10 ");
    }
}
class PureAbstract {
    public static void main(String[] args) {
        Movie obj=new Movie("RRR",8.4);
        obj.review();
     }
}
  

