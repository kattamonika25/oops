#include<iostream>
using namespace std;
class SuperClass{
    public:
        int pub=1;
    protected:
        int prot=2;
    private:
        int pvt=3;
    //access private member
    public:
    int getPVT(){
        return pvt;
    }
};
class PubDerived:public SuperClass{
    //access protected member from base
    public:
    int getprot(){
        return prot;
    }
};
class ProtDerived:protected SuperClass{
    //access public member from base
    public:
    int getpub(){
        return pub;
    }
    //access protected member from base
    int getprot(){
        return prot;
    }
};
class PvtDerived:private SuperClass{
    //access private member from base
    public:
    int getpub(){
        return pub;
    }
    //access protected member from base
    int getprot(){
        return prot;
    }
};
int main(){
    PubDerived obj1;
    ProtDerived obj2;
    PvtDerived obj3;
    cout << "Public Mode:"<<endl;
    cout<<"pub = "<< obj1.pub<<endl;
    cout<<"prot = "<< obj1.getprot()<<endl;
    cout<<"pvt = "<< obj1.getPVT()<<endl;

    cout<<"Protected Mode:"<<endl;
    cout<<"pub = "<< obj2.getpub()<<endl;
    cout<<"prot = "<< obj2.getprot()<<endl;
    cout<<"pvt is not Accessible"<<endl;

    cout<< "Private Mode:"<<endl;
    cout<<"pub = "<< obj3.getpub()<<endl;
    cout<<"prot = "<< obj3.getprot()<<endl;
    cout<<"pvt is not Accessible" <<endl;
}