import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
class ValidLogin {
    ValidLogin() {
        Frame f = new Frame();
        f.setTitle("Login Form");
        f.setSize(400, 400);
        f.setVisible(true);
        f.setBackground(Color.CYAN);
        f.setLayout(null);

        Label login = new Label("LOGIN");
        login.setBounds(140, 90, 60, 40);

        Label msg = new Label("Please enter your username and password");
        msg.setBounds(55, 130, 250, 30);


        Label l1 = new Label("Username");
        l1.setBounds(60, 170, 70, 30);
        TextField t1 = new TextField();
        t1.setBounds(150, 170, 150, 30);

        Label l2 = new Label("Password");
        l2.setBounds(60, 210, 70, 30);
        TextField t2 = new TextField();
        t2.setBounds(150, 210, 150, 30);
        t2.setEchoChar('*');

        Label forgot = new Label("forgot password?");
        forgot.setBounds(110, 250, 100, 30);

        Button b = new Button("Login");
        b.setBounds(130, 290, 60, 30);

        Label valid = new Label();
        valid.setBounds(50, 330, 300, 30);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (t1.getText().equals("student") && t2.getText().equals("21331A0583")){
                    valid.setText("Login Successful!");
                }
                else{
                    valid.setText("Invalid Username or Password.Please try again !");
                }
            }
        });
        f.add(login);
        f.add(msg);
        f.add(l1);
        f.add(t1);
        f.add(l2);
        f.add(t2);
        f.add(forgot);
        f.add(b);
        f.add(valid);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                f.dispose();
            }
        });

    }
    public static void main(String[] args) {
        ValidLogin obj = new ValidLogin();
    }
}


