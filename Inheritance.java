//Hybrid Inheritance
class GrandParent {
    int a=10;
}
//hierarchical
class Parent1 extends GrandParent {
    int b=15;
}
class Parent2 extends GrandParent {
    int c=20;
}
//single and multilevel
class Child1 extends GrandParent {
    int d=25;
}
class Grandson extends Child1 {
    int e=30;
}
//multiple Inheritance is not possible using classes

class Inheritance{
    public static void main(String[] args) {
    Grandson gs = new Grandson();
    Child1 c1 = new Child1();
    Parent1 p1 = new Parent1();
    Parent2 p2 = new Parent2();
    //using multilevel inheritance
    System.out.println("Sum of grandson accessed members : " + (gs.a+gs.d+gs.e));
    //using single inheritance
    System.out.println("Sum of child1 accessed members : " + (c1.a+c1.d));
    //using heirarchical inheritance
    System.out.println("Sum of parent1 and parent2 accessed members : " + (p1.a+p1.b+p2.c));
    }
}

