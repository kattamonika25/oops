class Boxing {
	public static void main(String[] args)
	{
        //Boxing
		System.out.println("Boxing:");
		Integer n1 = 25; 
		System.out.println("n1 = " + n1);

		Character ch = 'a'; 
		System.out.println("ch = " + ch);
		
        //Unboxing
		System.out.println("UnBoxing:");
        int n2 = n1;
        System.out.println("n2 = " + n2);

        char c = ch;
        System.out.println("c = " + c);

	}
}
