class Employee {
    void print() {
        System.out.println("Employee Details :");
    }

    void print(String name) {
        System.out.println("Employee name : " + name);
    }
 
    void print(String company, int id) {
        System.out.println("Company name: " + company);
        System.out.println("Employee id : " + id);
    }
    public static void main(String args[]) {
        Employee e = new Employee();
        e.print();
        e.print("Charan");
        e.print("HCL", 1036);
    }
}