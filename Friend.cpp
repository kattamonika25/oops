
#include<iostream>
using namespace std;
class Box {
    public:
    float length ;
    float width;
    float height;
  
    //member function defined inside class
    void boxArea()
    {
        cout << "The area of box is : "<< 2*((length * width) + (width * height) + (height * length))<< " sq.units" << endl;
    }

    //member function defined outside class
    void boxVolume();

    //friend function
    friend void displayBoxDimensions(Box b);

    inline void displayWelcomeMessage() {
         cout<<"Welcome"<<endl;
    }

};

void Box:: boxVolume()
{
    cout << "The volume of box is :" << length*width*height <<" cubic units" << endl ;
}

 void displayBoxDimensions(Box b) 
 {
    cout<<"Length of the Box :"<<b.length <<" units"<<endl;
    cout<<"Width of the Box :"<<b.width <<" units"<<endl;
    cout<<"Height of the Box :"<<b.height <<" units"<<endl;
 }

 int main() {
    Box gift;
    cout << "Enter length , width , height of the box : ";
    cin>> gift.length >> gift.width >> gift.height;
    gift.boxArea();
    gift.boxVolume();
    displayBoxDimensions(gift);
    gift.displayWelcomeMessage();

 }