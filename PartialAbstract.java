abstract class Lecturer {
    abstract void position();
    void teach() {
        System.out.println("Teach their specialised subject");
    }
} 
class SL extends Lecturer {
    void position() {
        System.out.println("Faculty at Institution level");
    }
}
class JL extends Lecturer {
    void position() {
        System.out.println("Faculty at College level");
    }
}
class PartialAbstract {
    public static void main(String[] args) {
    SL s = new SL();
    JL j = new JL();
    s.position();
    s.teach();
    j.position();
    j.teach();
}
}