#include<iostream>
using namespace std;
class Ancestor{
    public:
    int a=5;
    Ancestor() {
        cout<<"Ancestor called\n";
    }   
};
class Father : virtual public Ancestor {
    public:
    int b=10;
    Father() {
        cout<<"Father called\n";
    } 
};
class Mother : virtual public Ancestor{
    public:
    int c=15;
    Mother() {
        cout<<"Mother called\n";
    }
};
class Child : public Father,public Mother{
    public:
    int d=20;
    Child() {
        cout<<"Child called\n";
    } 
};
int main() {
    Child ch;
    cout<<"Sum : "<<(ch.a+ch.b+ch.c+ch.d)<<endl;
    return 0;
}