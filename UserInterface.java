import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JRadioButton;
public class UserInterface {
    UserInterface() {
        Frame f = new Frame();
        f.setTitle(" Event Registration Form");
        f.setSize(500,550);
        f.setVisible(true);
        f.setLayout(null);
        f.setBackground(Color.GREEN);
        Label l1 = new Label("Email Address ");
        l1.setBounds(50,50,100,30);
        TextField t1 = new TextField();
        t1.setBounds(150,50,250,30);

        Label l2 = new Label("First Name ");
        l2.setBounds(50, 100, 100, 30);
        TextField t2 = new TextField();
        t2.setBounds(150, 100, 250, 30);

        Label l3 = new Label("Last Name ");
        l3.setBounds(50,150,100,30);
        TextField t3 = new TextField();
        t3.setBounds(150,150,250,30);

        Label l4 = new Label("Register No.");
        l4.setBounds(50,200,100,30);
        TextField t4 = new TextField();
        t4.setBounds(150,200,250,30);

        Label l5 = new Label("Gender ");
        l5.setBounds(50, 250, 100, 30);
        /* Checkbox r1,r2;
        CheckboxGroup c = new CheckboxGroup();
        r1 = new Checkbox("Male", c, false);
        r1.setBounds(150,250, 120, 30);
        r2 = new Checkbox("Female", c, false);
        r2.setBounds(270,250,130,30);*/
        JRadioButton r1 = new JRadioButton("Male");
        r1.setBounds(150,250, 120, 30);
        r1.setSelected(false);
        JRadioButton r2 = new JRadioButton("Female");
        r2.setBounds(270,250,130,30);
        r2.setSelected(false);

        Label l6 = new Label("Event ");
        l6.setBounds(50, 300, 100, 30);
        Choice ch = new Choice();
        ch.setBounds(150,300,150,100);
        ch.add("Essay Writing");
        ch.add("Painting");
        ch.add("Coding");
        ch.add("Photography");
        ch.add("Techtalk");
        ch.add("Fastest Finger First");

        Label l7 = new Label("Phone no. ");
        l7.setBounds(50, 350,100, 30);
        TextField t5 = new TextField();
        t5.setBounds(150, 350, 250, 30);

        Button b1 = new Button("Submit");
        b1.setBounds(50,430,100,30);
        b1.setBackground(Color.WHITE);
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Your response has been recorded.");
            }
        });
        Button b2 = new Button("Clear form");
        b2.setBounds(300,430,100,30);
        b2.setBackground(Color.WHITE);
        b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String clear = " ";
                t1.setText(clear);
                t2.setText(clear);
                t3.setText(clear);
                t4.setText(clear);
                t5.setText(clear);
                r1.setSelected(false);
                r2.setSelected(false);
            }
        });

        f.add(l1);
        f.add(t1);
        f.add(l2);
        f.add(t2);
        f.add(l3);
        f.add(t3);
        f.add(l4);
        f.add(t4);  
        f.add(l5);
        f.add(r1);
        f.add(r2);
        f.add(l6);  
        f.add(l7);
        f.add(t5);
        f.add(b1);
        f.add(b2);
        f.add(ch);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                f.dispose();
            }
        });
    }
    public static void main(String[] args) {
        UserInterface obj = new UserInterface();
    }
}