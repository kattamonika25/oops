import java.util.HashMap;
import java.util.Map;

class MapDemo {
public static void main(String[] args) {
   // Create a map of generic type.	
   Map<Integer, String> sub = new HashMap<>();

  // Checking is map empty
   System.out.println("Is Map empty? " + sub.isEmpty());
  
   // Adding entries in the map.
   sub.put(1, "Maths");
   sub.put(2, "Abacus");
   sub.put(3, "Physics");
   System.out.println("Sub : " +sub);  
   
   //size of the map
   System.out.println("No. of entries in Map: " +sub.size());

   // Create another map.
   Map<Integer,String> sub2 = new HashMap<>();
   sub2.put(4, "Chemistry");
   sub2.put(5, "English");

   // Inserting elements of sub2 into sub.
   sub.putAll(sub2);
   System.out.println("Updated Sub : " +sub);

   // Removing
   sub.remove(3);
   System.out.println("After removing an entry of 3 : " +sub);
   sub.remove(4,"Chemistry");
   System.out.println("After removing Chemistry: " +sub);

   // Replacing 
   sub.replace(5, "Telugu");
   System.out.println("Updated Sub after replacing: " +sub);
   sub.replace(5, "Telugu", "Computers");
   System.out.println("Replacing Telugu with Computers : " +sub);

   //accessing value
   System.out.println("Value at beg : " + sub.get(1));
 
   //check for presence of keys and values
   System.out.println("Sub contains key2 : " +sub.containsKey(2));
   System.out.println("Sub contains value Hindi : " +sub.containsValue("Hindi"));

   /*cloning for HashMap
   sub.clone();
   System.out.println("Map after cloning : " + sub);*/

   // clearing
   sub.clear();
   System.out.println("Map after clearing : " + sub);
   
  }
}