#include<iostream>
using namespace std;
template<class Data>
class Template {
    public:
    void sub(Data num1,Data num2) {
        cout<<"Difference is : "<<num1 - num2<<endl;
    }
    void inc(Data num1) {
        cout<<"Increment of "<<num1 <<" : " << ++num1 <<endl;
    }
};
int main() {
    Template<float>obj;
    obj.sub(2.3,3.4);
    obj.inc(5);
    obj.sub(32,15);
    obj.inc(9.9);
}