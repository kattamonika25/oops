class AccessSpecifierDemo {
    private int priVar;
    protected int proVar;
    public int pubVar;

    public void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }

    public void getVar() {
        System.out.println("Value of Private Variable: " + priVar);
        System.out.println("Value of Protected Variable: " + proVar);
        System.out.println("Value of Public Variable: " + pubVar);
    }
    public static void main(String[] args) {
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(4,9,15);  
        obj.getVar();  
    }
}
