class Ant {
    void display() {
        System.out.println("Method of class Ant is called");
    }
}
class Bat extends Ant {
    void display() {
        System.out.println("Method of class Bat is called");
    }
}
class Cat extends Ant {
    void display() {
        System.out.println("Method of class Cat is called");
    }
} 
//an error arises
class Dog extends Bat,Cat {
    public static void main(String[] args) {
        Dog d = new Dog();
        d.display();
    }
}