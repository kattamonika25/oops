import java.lang.*;
import java.util.*;
class Calc {
    public static void main(String[] args) {
        System.out.print("Enter two integers : ");
        Scanner input = new Scanner(System.in);
        float f1 = input.nextFloat();
        float f2 = input.nextFloat();
        System.out.print("Enter the arithmetic operator : ");
        char op = input.next().charAt(0);
        if (op =='+')
            System.out.println("Sum = " + (f1+f2));
        else if (op=='-')
            System.out.println("Difference = " + (f1-f2));
        else if(op=='*')
            System.out.println("Product = " + (f1*f2));
        else if (op=='/')
            System.out.println("Quotient = " + (f1/f2));
        else if (op=='%')
            System.out.println("Remainder = " + (f1%f2));
        else 
            System.out.println("Invalid operator !");
    }
}
