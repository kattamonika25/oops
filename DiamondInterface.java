interface Ant {
    default void display() {
        System.out.println("Method of Ant called");
    }
}
interface Bat extends Ant {
    default void display() {
        Ant.super.display();
        System.out.println("Method of Bat called");
    }
}
interface  Cat extends Ant {
    default void display() {
        System.out.println("Method of Cat called");
    }
}
class Dog implements Bat,Cat {
    public void display() {
        Bat.super.display();
        Cat.super.display();
    }
}
class Diamond {
    public static void main(String[] args) {
        Dog obj = new Dog();
        obj.display();
    }
}