#include<iostream>
using namespace std;
class Video {
    public:
    virtual void review()=0;
};
class Movie : public Video {
    public:
        string title;
        float rating;
        
        Movie(string t, float r ) {
            title =  t;
            rating = r;
        }
        
        void review() {
            cout<<title<<" is an amazing movie with a rating of "<<rating<<" out of 10 "<<endl;
        }
};    
int main() {
    Movie obj("RRR",8.4);
    obj.review();
    return 0;
}