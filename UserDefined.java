import java.util.*;
class InvalidProductException extends Exception{
    public InvalidProductException(String msg){
        super(msg);
    }
}
class User {
   void productCheck(int weight) throws InvalidProductException {
    if(weight<100){
		throw new InvalidProductException("Product Invalid");
	}
    else {
        System.out.println("Valid Product received");
    }
   }
   
    public static void main(String args[]) throws InvalidProductException {
    	User obj = new User();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the Product Weight : ");
        int weight = sc.nextInt();
        obj.productCheck(weight);
    }
}