import java.io.*;
class Excep {
    public static void main(String[] args) {
        try {
            int n1 = 98;
            int n2 = 0;
            int res = n1 / n2;  //arithmetic exception
        }
         catch (ArithmeticException e) {
            System.out.println("Can't divide  by zero!");
        }
        try {
            int[] arr = {1, 2, 3, 4, 5};
            int x = arr[6];  //array index exception
            int y = arr[-1];  //array index exception
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Given Array index is not valid!");
        }
        try {
            File obj1 = new File("my_file.txt");
            FileReader obj2 = new FileReader(obj1);  //File Exception
        }
        catch (FileNotFoundException e) {
            System.out.println("Given File was not found!");
        }
        try {
            String str = "Exception Handling!";
            char c = str.charAt(25);   //String index Exception
        }        
        catch (StringIndexOutOfBoundsException e) {
            System.out.println("Given String Index is not valid!");
        }
    }
}
        
       