#include<iostream>
using namespace std;
class AccessSpecifierDemo {
  private:
    int priVar;
  protected:
    int proVar;
  public:
    int pubVar;

    void setVar(int priValue, int proValue, int pubValue) {
      priVar = priValue;
      proVar = proValue;
      pubVar = pubValue;
    }

    void getVar() {
      cout << "Value of Private Variable: " << priVar << endl;
      cout << "Value of Protected Variable: " << proVar << endl;
      cout << "Value of Public Variable: " << pubVar << endl;
    }
};
int main() {
    AccessSpecifierDemo obj;
    obj.setVar(4,9,15); 
    obj.getVar();  
    return 0;
}