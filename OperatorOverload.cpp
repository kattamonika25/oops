#include<iostream>
using namespace std;
class Fraction {
    private: 
        int num,den;
    public:
        Fraction(int n=0,int d=0) {
            num=n;
            den=d;
        }
  
    Fraction operator *(Fraction const &obj){
        Fraction res;
        res.num=num*obj.num;
        res.den=den*obj.den;
        return res;
    }
    void print() {
            cout<<"The fraction is  "<< num<<"/ "<<den<<endl;
    }
};

int main() {
    Fraction F1(24,5), F2(15,20);
    Fraction result = F1 * F2;
    result.print();
    return 0;
}