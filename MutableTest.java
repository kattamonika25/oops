public class MutableTest { 
  public static void main(String[] args) { 
    //Stringbuilder
    System.out.println("Mutability Test for Stringbuilder ");
    StringBuilder sb =new StringBuilder("123");  
    System.out.println(sb);
    System.out.println("Address of sb before changing : " + System.identityHashCode(sb));
    sb.reverse();
    System.out.println(sb);
    System.out.println("Address of sb after changing : " + System.identityHashCode(sb));  

    //StringBuffer
    System.out.println("Mutability Test for StringBuffer ");
    StringBuffer s = new StringBuffer(" Monika ");
    System.out.println(s);
    System.out.println("Address of s before changing : " + System.identityHashCode(s));
    s.insert(0, "Katta");
    System.out.println(s);
    System.out.println("Address of s after changing : " + System.identityHashCode(s));

    //for int
    System.out.println("Mutability Test for int ");
    int a = 10 ; 
    System.out.println(a);
    System.out.println("Address of a before changing : " + System.identityHashCode(a));
    a = a + 20;
    System.out.println(a);
    System.out.println("Address of a after changing : " + System.identityHashCode(a));  

    //for double
    System.out.println("Mutability Test for double ");
    double b = 1.0 ; 
    System.out.println(b);
    System.out.println("Address of b before changing : " + System.identityHashCode(b));
    b = b + 2.52;
    System.out.println(b);
    System.out.println("Address of b after changing : " + System.identityHashCode(b));  

    //string
    System.out.println("Mutability Test for String ");
    String str = "Moni" ; 
    System.out.println(str);
    System.out.println("Address of str before changing : " + System.identityHashCode(str));
    str = str + "ka";
    System.out.println(str);
    System.out.println("Address of str after changing : " + System.identityHashCode(str));  
  } 
}