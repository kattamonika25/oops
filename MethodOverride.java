class SchoolStudent {
    void study() {
        System.out.println("Do homework");
    }
}
class ComputerStudents extends SchoolStudent {
    void study() {
        System.out.println("Coding,Development");
    }
}
class MedicalStudents extends SchoolStudent {
    void study() {
        System.out.println("Labs,Experiments");
    }
}
class Override {
    public static void main(String[] args) {
        ComputerStudents cs = new ComputerStudents();
        MedicalStudents ms = new MedicalStudents();
        cs.study();
        ms.study();
    }
}