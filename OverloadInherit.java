class Employee {
    void print() {
        System.out.println("Employee Details :");
    }
}
class Developer extends Employee {
    void print(String name) {
        System.out.println("Employee name : " + name);
    }
}
class Manager extends Employee {
    void print(String company, int id) {
        System.out.println("Company name: " + company);
        System.out.println("Employee id : " + id);
    }
}
class OverloadInherit {
    public static void main(String[] args) {
        Developer d = new Developer();
        d.print();
        d.print("Charan");
        Manager m = new Manager();
        m.print();
        m.print("HCL", 1036);
    }
}