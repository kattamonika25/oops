import java.util.*;
class Multiply implements Runnable {
    void table(int n) {
        for(int i=1;i<=12;i++) {
            System.out.println(n + "*" + i + "=" + (n*i));
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
               e.printStackTrace();
            }
        }
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the table to display : ");
        int n = sc.nextInt();
        table(n);
    }
    public static void main(String[] args) {
        Multiply t1 = new Multiply();
        t1.run();
        Multiply t2 = new Multiply();
        t2.run();
    }
}