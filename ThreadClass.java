import java.util.Scanner;
class MyThread extends Thread {
    void table(int n) {
        for(int i=1;i<=12;i++) {
            System.out.println(n + "*" + i + "=" + (n*i));
            try {
                Thread.sleep(1500);
            }
            catch (InterruptedException e) {
               e.printStackTrace();
            }
        }
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the table to display : ");
        int n = sc.nextInt();
        table(n);
    }
    public static void main(String[] args) {
        MyThread obj1 = new MyThread();
        obj1.start();
        MyThread obj2 = new MyThread();
        obj2.start();
    }
}