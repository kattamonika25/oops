#include<stdio.h>
struct Employee{
    char name[30];
    int id;
    char dept[10];
    char designation[20];
    float salary;
}emp[10];

int main(){
    int n;
    FILE *f;
    f= fopen("Emp_details.txt","w");

    printf("Enter the no. of employees : ");
    scanf("%d",&n);
    for(int i=1;i<=n;i++){
        printf("Enter  the employee %d name,id,department,designation,salary : ",i);
        scanf("%s%d%s%s%f",emp[i].name,&emp[i].id,emp[i].dept,emp[i].designation,&emp[i].salary);
    }
    for(int j=1;j<=n;j++){
        fprintf(f,"\nEmployee %d details :",j);
        fprintf(f,"\n Employee Name: %s\n Id: %d\n Department:%s\n Designation:%s\n Salary:%.2f\n",emp[j].name,emp[j].id,emp[j].dept,emp[j].designation,emp[j].salary);
    }
    fclose(f);

    char c;
    FILE *fp;
    fp = fopen("Emp_details.txt", "r");
    if (fp == NULL) {
        printf("Content doesn't exist");
    }
    do {
        c = fgetc(fp);
        printf("%c", c);
    } while (c != EOF);
    fclose(fp);
}