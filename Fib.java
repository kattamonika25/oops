import MyPackage.Fibonacci;
import java.util.*;

public class Fib {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); 
        System.out.print("Enter n value : ");
        int n = input.nextInt();    
        System.out.println("Fibonacci Series till " + n + " terms: ");
        Fibonacci.fibSeries(n);
        System.out.println("\nThe "+ n + "th fibonacci number : " + Fibonacci.fib(n));
    }
}