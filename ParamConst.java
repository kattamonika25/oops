import java.lang.*;
class Student {
    String collegeName ;
    int collegeCode;
    String fullName;
    double semPercentage;
    //default constructor
    Student() {
        collegeName = "MVGR" ;
        collegeCode = 33 ;
        System.out.println("Default Constructor called");
        System.out.println("College Name : " + collegeName);
        System.out.println("College Code : " + collegeCode);
    }
    //parameterised constructor
    Student(String fname,double spercent) {
        fullName = fname;
        semPercentage = spercent;
        System.out.println("\nStudent Details");
        System.out.println("Full name : " + fullName);
        System.out.println("Sem Percentage : " + semPercentage);
    }
    
     protected void finalize()
    {
        System.out.println("Object destroyed by Garbage Collector");
    }

     public static void main(String[] args) 
    { 
        Student s1 = new Student();
        Student s2 = new Student("Cherry", 91.7);
        s1 = null;
        s2 = null;
        System.gc();
    }
}
   
