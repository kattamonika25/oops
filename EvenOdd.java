import java.lang.*;
import java.util.*;
class EvenOdd {
    public static void main(String[] args) {
        System.out.print("Enter any integer: ");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        evenodd(n);
    }
    public static void evenodd(int n){
        if (n%2==0){
            System.out.println(n + " is even");
        }
        else {
            System.out.println(n + " is odd");
        }
    }
}