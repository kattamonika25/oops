import java.io.*;
class JavaCopyFile {
    public static void main(String[] args) throws  IOException {
        FileInputStream fis =  null;
        FileOutputStream fos = null;
        try {
            fis =new FileInputStream("C:\\Users\\Katta Monika\\OneDrive\\Documents\\copy.txt");            
            fos = new FileOutputStream("C:\\Users\\Katta Monika\\OneDrive\\Documents\\Result.txt");
            int i;       
            while ((i = fis.read()) != -1) {
                fos.write(i);
            }
            System.out.println("copied the file successfully");
        }
        finally {
            if (fis != null) {
                fis.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }
}