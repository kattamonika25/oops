import java.util.*;
class Synchronize implements Runnable {
    synchronized void table(int n) {
        for(int i=1;i<=12;i++) {
            System.out.println(n + "*" + i + "=" + (n*i));
            try {
                Thread.sleep(500);
            }
            catch (InterruptedException e) {
               e.printStackTrace();
            }
        }
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the table to display : ");
        int n = sc.nextInt();
        table(n);
    }
    public static void main(String[] args) {
        Synchronize obj1 = new Synchronize();
        obj1.run();
        Synchronize obj2 = new Synchronize();
        obj2.run();
    }
}