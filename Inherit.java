class Employee {
    public String name = "Arun";
    protected int id = 542;
    private int salary = 50000 ;
    //Constructor
    Employee() {
        System.out.println("Employee called." );
    }
}
class Developer extends Employee {
    public String dev_name = "Kumar";
    protected int dev_id = 510;
    private int dev_salary = 45000 ;
    void display() {
        System.out.println("Employee name : " + name);
        System.out.println("Employee id : " + id );
        //System.out.println("Employee salary : " + salary);
        System.out.println("Developer name : " + dev_name);
        System.out.println("Developer id : " + dev_id );
        System.out.println("Developer salary : " + dev_salary);
        }
    public static void main(String[] args) {
        Developer d = new Developer();
        d.display();
    }  
}
// A class cannot be private or protected except nested class.