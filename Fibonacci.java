package MyPackage;
public class Fibonacci {
    //To display Fibonacci Series
    public static void fibSeries(int n) {
        int a = 0, b = 1 ;
         for (int i = 0; i < n; i++) {
            System.out.print(a + ", ");
            int c = a + b;
            a = b;
            b = c;
            }
    }
    //To display nth Fibonacci
    public static int fib(int n) {
        if (n <= 1) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }
}
