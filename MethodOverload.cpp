#include<iostream>
using namespace std;
class Shopping {
    public:
    void makePayment(string username,string password) {
        cout<<"Payment through internet banking:"<<endl;
        cout<<"Username : " << username <<endl;
        cout <<"Password : " << password <<endl;
    }
    void makePayment(long long mobileno,int password) {
        cout<<"Payment through UPI app:"<<endl;
        cout<<"Mobile number : " << mobileno <<endl;
        cout <<"Password : " << password <<endl;
    }
    void makePayment(long long cardNumber, int cvv, string name,string expireDate) {
        cout<<"Payment through Credit or Debit card:"<<endl;
        cout<<"Card Number : " << cardNumber <<endl;
        cout<<"CVV : " << cvv <<endl;
        cout<<"Person Name : " << name <<endl;
        cout<<"Expiry Date : " << expireDate <<endl;
    }
};
int main() {
    Shopping pay;
    pay.makePayment(9346859378LL,8341);
    pay.makePayment(6015110834015LL,513,"Dudu","11/2026");
    pay.makePayment("Candy","BubuDudu@143");
    return 0;
}
