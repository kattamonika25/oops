#include<iostream>
#include<string>
using namespace std;
class Student {
    public:
    string fullName;
    int rollNum;
    double semPercentage;
    string collegeName;
    int collegeCode;

    //parameterised constructor
    Student(string fname,int roll,double spercent,string cname,int ccode) {
        fullName = fname;
        rollNum = roll;
        semPercentage = spercent;
        collegeName = cname;
        collegeCode = ccode;
    }
    //methods
    void displayData()
    {
        cout<<"\n";
        cout<<"STUDENT DETAILS"<<endl;
        cout<<" Full Name : " <<fullName<<endl;
        cout<<" Roll No. : " <<rollNum<<endl;
        cout<<" Sem Percentage : " <<semPercentage<<endl;
        cout<<" College Name : " <<collegeName<<endl;
        cout<<" College Code : " <<collegeCode<<endl;
    }
    //Destructor
    ~Student() {
        cout<<"Deallocated"<<endl;
    }

};
int main() 
{ 
    Student s1("Ram",456,99,"MVGR",33),s2("Sita",257,89,"MVGR",33);
    s1.displayData();
    s2.displayData();
    return 0;
}

