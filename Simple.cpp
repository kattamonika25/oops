#include<iostream>
using namespace std;
class Mobile
{
    public:
    void voiceCall() {
        cout<<"You can make Voice call"<<endl;
    }

    void camera() {
        cout<<"16 Mega Pixel Camera\n";
    }
        
    void touchDisplay() {
        cout<<"5.5 inch touch display\n";
    }
};

class Vivo : public Mobile{
    public:
    Vivo() {
        cout<<"Vivo mobile.\n";
    }
};
int main() {
    Vivo v;
    v.voiceCall();
    v.camera();
    v.touchDisplay();
    return 0;
}