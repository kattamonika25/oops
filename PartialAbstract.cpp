#include<iostream>
using namespace std;
class Lecturer {
    public:
    virtual void position()=0;
    
    void teach() {
        cout<<"Teach their specialised subject"<<endl;
    }
};
class SL : public Lecturer {
    public:
    void position() {
        cout<<"Faculty at Institution level"<<endl;
    }
};
class JL : public Lecturer {
    public:
    void position() {
        cout<<"Faculty at College level"<<endl;
    }
};
int main() {
    SL s;
    JL j ;
    s.position();
    s.teach();
    j.position();
    j.teach();
    return 0;
}
