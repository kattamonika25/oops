//Hybrid inheritance
#include<iostream>
using namespace std;
class GrandParent {
    public:
    int a=10;
};
//hierarchical
class Parent1 : public GrandParent {
    public:
    int b=15;
};
class Parent2 : public GrandParent {
    public:
    int c=20;
};
//single and multilevel
class Child1 : public GrandParent {
    public:
    int d=25;
};
class Grandson : public Child1 {
    public:
    int e=30;
};
//multiple
class Child2 : public Parent1, public Parent2 {
    public:
    int f=9;
};
int main() {
    Grandson gs;
    Child2 c2;
    Child1 c1;
    Parent1 p1;
    Parent2 p2;
    //using multilevel inheritance
    cout<<"Sum of grandson accessed members : " <<(gs.a+gs.d+gs.e)<<endl;
    //using multiple inheritance
    cout<<"Sum of child2 accessed members : " <<(c2.b+c2.c+c2.f)<<endl;
    //using single inheritance
    cout<<"Sum of child1 accessed members : " <<(c1.a+c1.d)<<endl;
    //using heirarchical inheritance
    cout<<"Sum of parent1 and parent2 accessed members : "<<(p1.a+p1.b+p2.c)<<endl;

}