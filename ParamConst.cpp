#include<iostream>
#include<string>
using namespace std;
class Student {
    public:
    string collegeName ;
    int collegeCode;
    string fullName;
    double semPercentage;
    //default constructor
    Student() {
        collegeName = "MVGR" ;
        collegeCode = 33 ;
        cout<<"Default Constructor called"<<endl;
        cout<<"College Name : "<<collegeName<<endl;
        cout<<"College Code : "<<collegeCode<<endl;
    }
    //parameterised constructor
    Student(string fname,double spercent) {
        fullName = fname;
        semPercentage = spercent;
        cout<<"\nStudent Details"<<endl;
        cout<<"Full name : "<<fullName << endl;
        cout<<"Sem Percentage : " <<semPercentage << endl;
    }
    
    //destructor
    ~Student(){
        cout<<"\nDestructor called"<<endl;
    }

};
int main()
{
    Student s1;
    Student s2("Cherry", 91.7);
    
}