class Mobile
{
    void voiceCall() {
        System.out.println("You can make Voice call");
    }

    void camera() {
        System.out.println("16 Mega Pixel Camera");
    }
        
    void touchDisplay() {
        System.out.println("5.5 inch touch display");
    }
}

class Vivo extends Mobile{
    Vivo() {
        System.out.println("Vivo mobile.");
    }
}
class Simple {
    public static void main(String[] args) {
    Vivo v = new Vivo();
    v.voiceCall();
    v.camera();
    v.touchDisplay();
    }
}