import java.nio.*;

class BufferExample {
   public static void main(String[] args) {
      // Create a buffer with a capacity of 10 bytes
      ByteBuffer buffer = ByteBuffer.allocate(10);

      // Write some data to the buffer
      buffer.put((byte) 'h');
      buffer.put((byte) 'e');
      buffer.put((byte) 'l');
      buffer.put((byte) 'l');
      buffer.put((byte) 'o');

      // Prints the position of buffer
      System.out.println("Position after data is written into buffer : " + buffer.position());
     
      // flips the buffer
      buffer.flip();

      // Read the data from the buffer
      System.out.println("Reading buffer contents:");
      while (buffer.hasRemaining()) {
         System.out.println((char) buffer.get());
      }

      // Reset the position to the beginning of the buffer
      buffer.rewind();

      // Prints the position of buffer
      System.out.println("Position of buffer after rewind : " + buffer.position());
   }
}

