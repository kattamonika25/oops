import java.util.*;
public class CollectionsDemo {
    public static void main(String args[]) {
//LIST COLLECTION
        Vector<Integer> vectorObject = new Vector<Integer>();
        System.out.println("\n**Vector Operations**");
        //isEmpty()
        System.out.println("Is vector object empty: " +vectorObject.isEmpty());

        //adding
        vectorObject.add(3);
        vectorObject.add(9);
        vectorObject.add(0,5);
        vectorObject.addElement(2);
        System.out.println("Values in Vecor object" +vectorObject);

        //capacity
        System.out.println("current capacity of Vector object is " +vectorObject.capacity());
        
        //elementAt()
        System.out.println("Element at position 2 : " +vectorObject.elementAt(2) );
        
        //Accessing first and Last Elements
        System.out.println("value at first index : " +vectorObject.firstElement());
        System.out.println("value at last index : " +vectorObject.lastElement());

        //get() and set()
        System.out.println("value at  index 2: " +vectorObject.get(2));
        vectorObject.set(3, 15);
        System.out.println("Values in vector after replacing :" +vectorObject);

        //indexOf()
        System.out.println("Index of element 9 is : " +vectorObject.indexOf(9));

        //remove()
        System.out.println("removed element at index 2  : " +vectorObject.remove(2));
        
        //cloning
        vectorObject.clone();
        System.out.println("Values in Vector object are : " +vectorObject);
        
        //contains()
        System.out.println("vector object contains elemnent 10? : " +vectorObject.contains(10));

        //clearing
        vectorObject.clear();
        System.out.println("After clearing : " +vectorObject);

//QUEUE COLLECTION
        Queue<String> queueObject = new PriorityQueue<>();
        System.out.println("\n**Queue Operations**");
        //isEmpty()
        System.out.println("Is queue empty: " +queueObject.isEmpty());

        //add() and offer()
        queueObject.add("Java");
        queueObject.add("Python");
        queueObject.add("C++");
        queueObject.add("OOPS");
        System.out.println("Queue : " +queueObject);
        queueObject.offer("C");
        System.out.println("Updated Queue : " +queueObject);

        //size()
        System.out.println("Size of the Queue : " +queueObject.size());

        //peek() and element()
        System.out.println("Peek element : " +queueObject.peek());
        System.out.println("Peek element : " +queueObject.element());
        
        //remove() and poll()
        System.out.println("Removed element : " +queueObject.remove());
        System.out.println("Removed element using peek() : " +queueObject.poll());

//SET COLLECTION       
        Set <String> cities = new HashSet<String>(); 
        System.out.println("\n**Set Operations**");
        //isEmpty()
        System.out.println("Is Set empty: " +cities.isEmpty());

        //add() and addAll()
        cities.add("Bangaluru"); 
        cities.add("Chennai"); 
        Set <String> cities1 = new HashSet<String>();
        cities1.add("Hyderabad"); 
        cities1.add("Kolkata"); 
        cities.addAll(cities1);
        System.out.println("HashSet: " + cities); 

        //size()
        System.out.println("Size of set : " + cities.size());

        //contains ()
        System.out.println("Cities contains Mumbai : " + cities.contains("Mumbai"));

        //hashCode()
        System.out.println("The hashcode is : " +cities.hashCode());

        //remove() and removeAll()
        cities.remove("Chennai");
        System.out.println("Cities after removing Chennai :" +cities );
        cities.removeAll(cities1);
        System.out.println("Removing cities1 : " +cities);

        //clear()
        cities.clear();
        System.out.println("Final set: " +cities);
    }
}
